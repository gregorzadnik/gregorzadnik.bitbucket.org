
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var idji = ["4e7b8f50-f41d-485e-a311-fb7d182097df", "d3acd2f1-7fd6-4e79-9199-1dba31c7ccc5", "06971d48-4a6f-498a-8a79-3a168339531f"];

/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */


function preberiEHRodBolnika() {
	sessionId = getSessionId();

	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-success fade-in'>Bolnik '" + party.firstNames + " " +
          party.lastNames + "', ki se je rodil '" + party.dateOfBirth +
          "'.</span>");
			},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
			}
		});
	}
}

function pobrisiNoveIDje(){
    document.getElementById("noviIDji").innerHTML = "";
	document.getElementById("podatki").innerHTML = "";
    
}

function generirajPodatke(stPacienta) {
	sessionId = getSessionId();
    var ehrId = "";
	var imena = ["Gregor", "Tone", "Danny"];
	var priimki = ["Zadnik", "Pavček", "DeVito"];
    var datumiRojstva = ["1998-05-13T00:00:00.000Z", "1958-08-24T00:00:00.000Z", "1966-10-12T00:00:00.000Z"];
    var i = stPacienta-1;
    //zapisiMeritve(ehrId, "2018-05-20T20:00Z", "173", "85.00", "39.0", "140", "60", "96", "Tony Stark");
    $.ajaxSetup({
        headers: {"Ehr-Session": sessionId}
	});
	$.ajax({
	    url: baseUrl + "/ehr",
	    type: 'POST',
	    success: function (data) {
	            ehrId = data.ehrId;
	        var partyData = {
	            firstNames: imena[i],
	            lastNames: priimki[i],
	            dateOfBirth: datumiRojstva[i],
	            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
	        };
	        $.ajax({
	            url: baseUrl + "/demographics/party",
	            type: 'POST',
	            contentType: 'application/json',
	            data: JSON.stringify(partyData),
	            success: function (party) {
	                if (party.action == 'CREATE') {
                        $("#kreirajSporocilo").html("<span class='obvestilo label label-success fade-in'>Podatki vpisani</span>");
                        $("#m" + stPacienta).val(ehrId);
                        idji[i] = ehrId;
                        document.getElementById("noviIDji").innerHTML += imena[i] + " " + priimki[i] +  ": " + ehrId + "<br>";
                        console.log("Nov id " + stPacienta + ": " + ehrId);
                    }
	            },
	            error: function(err) {
	                $("#kreirajSporocilo").html("<span class='obvestilo label " +
                        "label-danger fade-in'>Napaka '" +
                        JSON.parse(err.responseText).userMessage + "'!");
	            }
	        });
    		
    		if(stPacienta == 1){
    		    zapisiMeritve(ehrId, "2018-05-20T20:00Z", "183", "69.00", "38.0", "120", "92", "98", "Tony Stark");
    		    zapisiMeritve(ehrId, "2019-06-03T15:00Z", "184", "69.50", "37.7", "124", "90", "97", "Tony Stark");
    		    zapisiMeritve(ehrId, "2020-09-05T17:00Z", "185", "72.50", "37.9", "117", "94", "97", "Tony Stark");
    		    document.getElementById("podatki").innerHTML += "<b>Meritve za pacienta Gregor Zadnik:</b> <br>";
    		    document.getElementById("podatki").innerHTML += "ID: " + ehrId + "<br> čas: 2018-05-20T20:00Z <br> višina: 183 <br> teža: 69.00 <br> temperatura: 38.0 <br> sistolični tlak: 120 <br> diastolični tlak: 92 <br> nasičenost krvi s kisikom: 98 <br> merilec: Tony Stark <br><br>";
    		    document.getElementById("podatki").innerHTML += "ID: " + ehrId + "<br> čas: 2019-06-03T15:00Z <br> višina: 184 <br> teža: 69.50 <br> temperatura: 37.7 <br> sistolični tlak: 124 <br> diastolični tlak: 90 <br> nasičenost krvi s kisikom: 97 <br> merilec: Tony Stark <br><br>";
    		    document.getElementById("podatki").innerHTML += "ID: " + ehrId + "<br> čas: 2020-09-05T17:00Z <br> višina: 185 <br> teža: 72.50 <br> temperatura: 37.9 <br> sistolični tlak: 117 <br> diastolični tlak: 94 <br> nasičenost krvi s kisikom: 97 <br> merilec: Tony Stark <br><br>";
    		}
    		else if(stPacienta == 2){
    		   zapisiMeritve(ehrId, "2018-05-20T20:00Z", "175", "70.00", "38.5", "118", "50", "97", "Tony Stark");
    		   zapisiMeritve(ehrId, "2018-06-26T19:00Z", "175", "73.00", "38.4", "125", "70", "96", "Frank The Tank");
    		   zapisiMeritve(ehrId, "2018-07-17T13:00Z", "175", "75.00", "38.9", "135", "100", "97", "Frank The Tank");
    		   document.getElementById("podatki").innerHTML += "<b>Meritve za pacienta Tone Pavček:</b> <br>";
			   document.getElementById("podatki").innerHTML += "ID: " + ehrId + "<br> čas: 2018-05-20T20:00Z <br> višina: 175 <br> teža: 70.00 <br> temperatura: 38.5 <br> sistolični tlak: 118 <br> diastolični tlak: 50 <br> nasičenost krvi s kisikom: 97 <br> merilec: Tony Stark <br><br>";
			   document.getElementById("podatki").innerHTML += "ID: " + ehrId + "<br> čas: 2018-06-26T19:00Z <br> višina: 175 <br> teža: 73.00 <br> temperatura: 38.4 <br> sistolični tlak: 125 <br> diastolični tlak: 70 <br> nasičenost krvi s kisikom: 96 <br> merilec: Frank The Tank <br><br>";
			   document.getElementById("podatki").innerHTML += "ID: " + ehrId + "<br> čas: 2018-07-17T13:00Z <br> višina: 175 <br> teža: 75.00 <br> temperatura: 38.9 <br> sistolični tlak: 135 <br> diastolični tlak: 100 <br> nasičenost krvi s kisikom: 97 <br> merilec: Frank The Tank <br><br>";
    		}
    		else if(stPacienta == 3){
    		    zapisiMeritve(ehrId, "2018-05-20T20:00Z", "173", "85.00", "39.0", "140", "60", "92", "Tony Stark");
    		    zapisiMeritve(ehrId, "2017-12-19T20:30Z", "174", "90.00", "40.0", "150", "50", "93", "Mary Poppins");
    		    zapisiMeritve(ehrId, "2020-11-15T20:00Z", "170", "110.50", "42.0", "160", "40", "94", "Mary Poppins");
    		    document.getElementById("podatki").innerHTML += "<b>Meritve za pacienta Danny DeVito:</b> <br>";
    		    document.getElementById("podatki").innerHTML += "ID: " + ehrId + "<br> čas: 2018-05-20T20:00Z <br> višina: 173 <br> teža: 85.00 <br> temperatura: 39.0 <br> sistolični tlak: 140 <br> diastolični tlak: 60 <br> nasičenost krvi s kisikom: 92 <br> merilec: Tony Stark <br><br>";
    		    document.getElementById("podatki").innerHTML += "ID: " + ehrId + "<br> čas: 2017-12-19T20:30Z <br> višina: 174 <br> teža: 90.00 <br> temperatura: 40.0 <br> sistolični tlak: 150 <br> diastolični tlak: 50 <br> nasičenost krvi s kisikom: 93 <br> merilec: Mary Poppins <br><br>";
    		    document.getElementById("podatki").innerHTML += "ID: " + ehrId + "<br> čas: 2020-11-15T20:00Z <br> višina: 170 <br> teža: 110.50 <br> temperatura: 42.0 <br> sistolični tlak: 160 <br> diastolični tlak: 40 <br> nasičenost krvi s kisikom: 94 <br> merilec: Mary Poppins <br><br>";
    		}
    		console.log("Tlak in teža za " + stPacienta + ": ");
	    }
	});
	return ehrId;
}

function zapisiMeritve(ehrId, cas, visina, teza, temperatura, sTlak, dTlak, nasicenost, merilec){
    var podatki = {
    			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
          // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
    		    "ctx/language": "en",
    		    "ctx/territory": "SI",
    		    "ctx/time": cas,
    		    "vital_signs/height_length/any_event/body_height_length": visina,
    		    "vital_signs/body_weight/any_event/body_weight": teza,
    		   	"vital_signs/body_temperature/any_event/temperature|magnitude": temperatura,
    		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
    		    "vital_signs/blood_pressure/any_event/systolic": sTlak,
    		    "vital_signs/blood_pressure/any_event/diastolic": dTlak,
    		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenost
    		};
    		var parametriZahteve = {
    		    ehrId: ehrId,
    		    templateId: 'Vital Signs',
    		    format: 'FLAT',
    		    committer: merilec
    		};
    		$.ajax({
    		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
    		    type: 'POST',
    		    contentType: 'application/json',
    		    data: JSON.stringify(podatki),
    		    success: function (res) {
    		        $("#dodajMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-success fade-in'>" +
                  res.meta.href + ".</span>");
    		    },
    		    error: function(err) {
    		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
                "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                JSON.parse(err.responseText).userMessage + "'!");
    		    }
    		});
}

function izracunajMoznostKapi(){
	sessionId = getSessionId();
	
	var ehrId = $("#meritveVitalnihZnakovEHRid").val();
	if (!ehrId || ehrId.trim().length == 0){
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
		    url: baseUrl + "/view/" + ehrId + "/" + "weight",
		    type: 'GET',
		    headers: {"Ehr-Session": sessionId},
		    success: function (res) {
		    	if (res.length > 0) {
		    		$.ajax({
					    url: baseUrl + "/view/" + ehrId + "/blood_pressure",
					    type: 'GET',
					    data: {
					        from: '2014-3-1'
					    },
					    headers: {
					        "Ehr-Session": sessionId
					    },
					    success: function (tlaki) {
					    	if(tlaki.length > 0){
					    		var sistolicni = tlaki[0].systolic;
					    		var diastolicni = tlaki[0].diastolic;
					    		var teza = res[0].weight;
					    		var cas = res[0].time;
					    		var moznost = (sistolicni/4 - 23) + (diastolicni/4 - 24) + (teza/4 - 20);
					    		if(moznost > 100)
					    			moznost = 100;
					    		else if(moznost < 0){
					    			moznost = 1.5;
					    		}
					    		if(moznost >= 50){
					    			zemljevid = true;
					    		}
					    		document.getElementById("rezultatMeritveVitalnihZnakov").innerHTML = ("Za meritve tlaka <b>" + sistolicni + "/" + diastolicni + " " + tlaki[0].unit + "</b> in teže <b>" + 
					    		teza + " kg</b> ob <b>" + cas + "</b> je moznost srčne kapi v naslednjih petih letih: <br>" + "<h1>" + moznost + 
					    		"%</h1><br> Če menite, da je verjetnost previsoka, svetujemo, da obiščete eno izmed bližnjih bolnišnic, ki so prikazane na dnu strani.");
						       /* for (var i in res) {
						            console.log(res[i].time + ': ' + res[i].systolic + "/" + res[i].diastolic + " " + res[i].unit);
						            document.getElementById("podatki").innerHTML += res[i].time + ': ' + res[i].systolic + "/" + res[i].diastolic + " " + res[i].unit + "<br>";
						        }*/
						        var ctx = document.getElementById("myChart");
								var myChart = new Chart(ctx, {
								    type: 'bar',
								    data: {
								        labels: [""],
								        datasets: [{
								            label: '% možnosti srčne kapi v naslednjih 5 letih',
								            data: [moznost],
								            backgroundColor: [
								                'rgba(255, 99, 132, 0.2)',
								                'rgba(54, 162, 235, 0.2)',
								                'rgba(255, 206, 86, 0.2)',
								                'rgba(75, 192, 192, 0.2)',
								                'rgba(153, 102, 255, 0.2)',
								                'rgba(255, 159, 64, 0.2)'
								            ],
								            borderColor: [
								                'rgba(255,99,132,1)',
								                'rgba(54, 162, 235, 1)',
								                'rgba(255, 206, 86, 1)',
								                'rgba(75, 192, 192, 1)',
								                'rgba(153, 102, 255, 1)',
								                'rgba(255, 159, 64, 1)'
								            ],
								            borderWidth: 1
								        }]
								    },
								    options: {
								        scales: {
								            yAxes: [{
								                ticks: {
								                    beginAtZero:true
								                }
								            }]
								        }
								    }
								});
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
				    "<span class='obvestilo label label-warning fade-in'>" +
				    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
				  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
				  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
		    	}
			    /*	var results = "<table class='table table-striped " +
        "table-hover'><tr><th>Datum in ura</th>" +
        "<th class='text-right'>Telesna teža</th></tr>";
			        for (var i in res) {
			            results += "<tr><td>" + res[i].time +
              "</td><td class='text-right'>" + res[i].weight + " " 	+
              res[i].unit + "</td></tr>";
			        }
			        results += "</table>";
			        $("#rezultatMeritveVitalnihZnakov").append(results);
		    	}*/ else {
		    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
        "<span class='obvestilo label label-warning fade-in'>" +
        "Ni podatkov!</span>");
		    	}
		    },
		    error: function() {
		    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
      "<span class='obvestilo label label-danger fade-in'>Napaka '" +
      JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	}
	
	
}

function initMap() {
              if (navigator.geolocation) {
                try {
                  navigator.geolocation.getCurrentPosition(function(position) {
                    var myLocation = {
                      lat: position.coords.latitude,
                      lng: position.coords.longitude
                    };
                    setPos(myLocation);
                  });
                } catch (err) {
                  var myLocation = {
                    lat: 46.0552778,
                    lng: 14.5144444
                  };
                  setPos(myLocation);
                }
              } else {
                var myLocation = {
                  lat: 46.0552778,
                  lng: 14.5144444
                };
                setPos(myLocation);
              }
            }

            function setPos(myLocation) {
              map = new google.maps.Map(document.getElementById('map'), {
                center: myLocation,
                zoom: 10
              });

              var service = new google.maps.places.PlacesService(map);
              service.nearbySearch({
                location: myLocation,
                radius: 4000,
                types: ['hospital']
              }, processResults);

            }

            function processResults(results, status, pagination) {
              if (status !== google.maps.places.PlacesServiceStatus.OK) {
                return;
              } else {
                createMarkers(results);

              }
            }

            function createMarkers(places) {
              var bounds = new google.maps.LatLngBounds();
              var placesList = document.getElementById('places');

              for (var i = 0, place; place = places[i]; i++) {
                var image = {
                  url: place.icon,
                  size: new google.maps.Size(71, 71),
                  origin: new google.maps.Point(0, 0),
                  anchor: new google.maps.Point(17, 34),
                  scaledSize: new google.maps.Size(25, 25)
                };

                var marker = new google.maps.Marker({
                  map: map,
                  icon: image,
                  title: place.name,
                  animation: google.maps.Animation.DROP,
                  position: place.geometry.location
                });

                bounds.extend(place.geometry.location);
              }
              map.fitBounds(bounds);
            }



$(document).ready(function() {

  /**
   * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju
   * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
   * padajočega menuja (npr. Pujsa Pepa).
   */
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajDatumRojstva").val(podatki[2]);
  });

  /**
   * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,
   * ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Dejan Lavbič, Pujsa Pepa, Ata Smrk)
   */
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});

});
// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
